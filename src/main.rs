// SPDX-FileCopyrightText: 2020 Florian Warzecha <liketechnik@disroot.org>
//
// SPDX-License-Identifier: MPL-2.0

use std::process::Command;
use std::path::PathBuf;
use std::io::BufRead;
use std::io::BufReader;
use std::fs::File;

use anyhow::{anyhow, Result};

fn main() -> Result<()> {
    let f = File::open("downloads.txt").map_err(|e| anyhow!("Could not open 'downloads.txt': {:?}", e))?;
    let f = BufReader::new(f);


    for line in f.lines() {
        let line = line?;

        let mut split_iter = line.split(";");
        let url = split_iter.next().ok_or_else(|| anyhow!("Expected two parts seperated with semicolon. Line was: {}", &line))?;
        let file = PathBuf::from(split_iter.next().ok_or_else(|| anyhow!("Expected two parts seperated with semicolon. Line was: {}", &line))?);

        if file.is_file() {
            println!("Skipping {} because the file already exists!", file.to_string_lossy());
        } else {
            println!("Starting download of {} to {}:\n", url, file.to_string_lossy());
            let cmd = Command::new("ffmpeg")
                .arg("-hwaccel")
                .arg("auto")
                .arg("-i")
                .arg(url)
                .arg("-c:v")
                .arg("libvpx-vp9")
                .arg("-crf")
                .arg("10")
                .arg("-b:v")
                .arg("0")
                .arg("-row-mt")
                .arg("1")
                .arg("-c:a")
                .arg("libopus")
                .arg("-application")
                .arg("voip")
                .arg("-compression_level")
                .arg("10")
                .arg(file.to_str().ok_or_else(|| anyhow!("Path contains non utf-8 values"))?)
                .status()?;

            if !cmd.success() {
                return Err(anyhow!("Failed to download {} from {}!", file.to_string_lossy(), url));
            }

            println!("--\n\n");
        }
    }
    
    Ok(())
}
